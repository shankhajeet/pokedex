## PokeDex

It is a basic **[React App](https://reactjs.org/)** bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It is listing pokemons and showing basic details about them. I have used **[PokeApi](https://pokeapi.co/)** to fetch the list of pokemons and other detail specs related to them. I am also inspired from a Youtube video by **[Chris Stayte](https://www.youtube.com/channel/UC2NGv5afGSZXFHtNo_EJLdQ)** called **[Fun With API's - Pokedex (Using React)](https://www.youtube.com/watch?v=XehSJF85F38)** for this project of mine.

In it you can **Search and View List of Pokemons** that are out there and also know about their Unique powers and others details in **Detail Page**. The site is compeltely **Responsive** for both Desktop and Mobile versions.

It is a very basic **[React App](https://reactjs.org/)** project to understand the basics of **[React](https://reactjs.org/)** with some of the packages that i have used - <br />

1. **[React Router Dom](https://www.npmjs.com/package/react-router-dom)** for the routing within the website.
2. **[Styled Components](https://styled-components.com/)** for styling components within the jsx or js files.
3. **[Bootstrap](https://getbootstrap.com/)** for the basic css and javascript framework.
4. **[Axios](https://www.npmjs.com/package/axios)** for the Api calls. 

And many more packages.

## Screen Shots

## `Home Screen`
**Desktop View:**<br />
![](screenshots/desktop/home.png)
**Mobile View:**<br />
![](screenshots/mobile/home.png)

## `Search Screen`
**Desktop View:**<br />
![](screenshots/desktop/search.png)
**Mobile View:**<br />
![](screenshots/mobile/search.png)

## `Detail Screen`
**Desktop View:**<br />
![](screenshots/desktop/detail.png)
**Mobile View:**<br />
![](screenshots/mobile/detail.png)


## How To Run

## `Clone/Download`

Clone or Donwload the porject from **[gitlab](https://gitlab.com/shankhajeet/pokedex)**

## `npm install`

Open the project directory on your teminal and run <br />
**$ npm install**


### `npm start`

After the installation is complete run <br />
**$ npm start**

It will run the project in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser and enjoy.

