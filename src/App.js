import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import NavBar from "./components/layout/navBar/NavBar";
import Dashboard from "./components/layout/dashboard/Dashboard";
import PokemonDetail from "./components/pokemon/pokemonDetail/PokemonDetail"

class App extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <Router>
        <div className="App">
          <NavBar />
          <div className="container" >
            <Switch>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/pokemon/:pokemonIndex" component={PokemonDetail} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
