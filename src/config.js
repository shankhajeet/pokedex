const PokeApi = {
    url: 'https://pokeapi.co/api/v2/pokemon/',
    image: 'https://github.com/PokeAPI/sprites/blob/master/sprites/pokemon/',
    specs: 'https://pokeapi.co/api/v2/pokemon-species/'
} 
export default PokeApi;