import React, { Component } from "react";
import PokemonListing from "../../pokemon/pokemonListing/PokemonListing";
import PokemonSearch from "../../pokemon/pokemonSearch/PokemonSearch";
import PokeApi from "../../../config";

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      Pokemons: [],
      Search: "",
    };
  }

  componentDidMount() {
    // Get pokemon array with javascript fetch only
    fetch(`${PokeApi.url}?offset=0&limit=100`)
      .then((response) => response.json())
      .then((data) => this.setState({ Pokemons: data.results }));
  }

  render() {
    // Filtered Array
    const filterSearch = this.state.Pokemons.filter((Pokemon) =>
      Pokemon.name.toLowerCase().includes(this.state.Search.toLowerCase())
    );

    return (
      <div className="Dashboard">
        <PokemonSearch
          placeholder="Search pokemon by name ..."
          onHandelChange={(e) => this.setState({ Search: e.target.value })}
        />
        <PokemonListing Pokemons={filterSearch} />
      </div>
    );
  }
}

export default Dashboard;
