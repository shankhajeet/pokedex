import React, { Component } from "react";
import './NavBar.css'
import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #232323;
  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
    color: #232323
  }
`;

class NavBar extends Component {
  render() {
    return (
        <div>
            <nav className="navbar">
              <StyledLink to="/" >
                <div className="siteName_wrapper">
                    <img src="images/logo/pokedex-logo.png" alt="" className="siteLogo" />
                    <h1>PokeDex React</h1>
                </div>
              </StyledLink>
            </nav>
        </div>
    );
  }
}

export default NavBar;
