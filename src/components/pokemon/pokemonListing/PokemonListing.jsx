import React, { Component } from "react";
import "./PokemonListing.css";
import PokemonCards from "../pokemonCards/PokemonCards";

class PokemonListing extends Component {
  render() {
    return (
      <div className="row my-5 py-5">
        {this.props.Pokemons.map((Pokemon) => (
          <PokemonCards Pokemon={Pokemon} key={Pokemon.name} />
        ))}
      </div>
    );
  }
}

export default PokemonListing;
