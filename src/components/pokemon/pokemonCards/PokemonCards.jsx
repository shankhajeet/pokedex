import React, { Component } from "react";
import PokeApi from "../../../config";
import "./PokemonCards.css";
import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #232323;
  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;

class PokemonCards extends Component {
  constructor() {
    super();
    this.state = {
      index: "",
      name: "",
      imageUrl: "",
      imageLoading: true,
      toManyRequests: false,
    };
  }

  componentDidMount() {
    const { Pokemon } = this.props;
    const index = Pokemon.url.split("/")[Pokemon.url.split("/").length - 2];
    const imageUrl = `${PokeApi.image}${index}.png?raw=true`;
    this.setState({
      index: index,
      name: Pokemon.name,
      imageUrl: imageUrl,
    });
  }

  render() {
    return (
      <div className="col-sm-6 col-md-3 mb-5">
        <StyledLink to={`pokemon/${this.state.index}`}>
          <div className="card single_card">
            <h5 className="card-header">{this.state.index}</h5>
            <div className="card-body d-flex justify-content-center align-items-center flex-column">
              <img
                src={this.state.imageUrl}
                alt=""
                onLoad={() => {
                  this.setState({ imageLoading: false });
                }}
                onError={() => this.setState({ toManyRequests: true })}
                className={
                  this.state.imageLoading
                    ? "img-fluid mx-auto pokemon_card_image d-none"
                    : "img-fluid mx-auto pokemon_card_image d-block"
                }
              />
              {this.state.imageLoading ? (
                <div className="spinner-grow text-danger my-4" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              ) : null}
              {this.state.toManyRequests ? (
                <h6 className="mx-auto my-3">
                  <span className="badge badge-danger">To Many Requests</span>
                </h6>
              ) : null}
              <h4 className="card-title text-center text-capitalize">
                {" "}
                {this.state.name}{" "}
              </h4>
            </div>
          </div>
        </StyledLink>
      </div>
    );
  }
}

export default PokemonCards;
