import React, { Component } from "react";
import "./PokemonSearch.css";

class PokemonSearch extends Component {
  render() {
    return (
      <div className="pokemon_search_input_wrapper">
        <input
          type="search"
          placeholder={this.props.placeholder}
          onChange={this.props.onHandelChange}
          className="pokemon_search_input"
        />
      </div>
    );
  }
}

export default PokemonSearch;
